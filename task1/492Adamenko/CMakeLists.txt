set(SRC_FILES
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    common/DebugOutput.cpp

    common/Application.hpp
    common/Camera.hpp
    common/Mesh.hpp
    common/ShaderProgram.hpp
    common/DebugOutput.h

    Main.h
    Main.cpp
)

include_directories(common)

MAKE_TASK(492Adamenko 1 "${SRC_FILES}")
