#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "PerlinNoise.h"

#include <iostream>
#include <vector>
#include <cassert>
#include <random>

using std::vector;
using std::pair;

std::vector<glm::vec3> getTriangleNormals(const std::vector<glm::vec3> & vertices) {
    std::vector<glm::vec3> result;
    result.reserve(3);
    result.push_back(normalize(cross(vertices[2] - vertices[0], vertices[1] - vertices[0])));
    result.push_back(normalize(cross(vertices[0] - vertices[1], vertices[2] - vertices[1])));
    result.push_back(normalize(cross(vertices[1] - vertices[2], vertices[0] - vertices[2])));
    return result;
}

MeshPtr buildTerrainWithHeights(const vector<vector<float> > & heights, pair<float, float> xy_size) {
    assert(heights.size() > 0 && heights[0].size() > 0);
    
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;
    
    float x_step = xy_size.first / float(heights.size());
    float y_step = xy_size.second / float(heights[0].size());
    
    for(int ix = 0; ix < heights.size() - 1; ++ix) {
        float x_crd = ix * x_step;
        for(int iy = 0; iy < heights[0].size() - 1; ++iy) {
            float y_crd = iy * y_step;
            std::vector<glm::vec3> triangle;
            std::vector<glm::vec3> triangle_normals;
            
            glm::vec3 p1 = glm::vec3(x_crd, y_crd, heights[ix][iy]);
            glm::vec3 p2 = glm::vec3(x_crd + x_step, y_crd, heights[ix + 1][iy]);
            glm::vec3 p3 = glm::vec3(x_crd + x_step, y_crd + y_step, heights[ix + 1][iy + 1]);
            glm::vec3 p4 = glm::vec3(x_crd, y_crd + y_step, heights[ix][iy + 1]);
            
            // T1
            triangle = {p1, p3, p2};
            triangle_normals = getTriangleNormals(triangle);
            vertices.insert(vertices.end(), triangle.begin(), triangle.end());
            normals.insert(normals.end(), triangle_normals.begin(), triangle_normals.end());
            
            texcoords.push_back(glm::vec2(0.0, 1.0));
            texcoords.push_back(glm::vec2(1.0, 0.0));
            texcoords.push_back(glm::vec2(1.0, 1.0));
            
            // T2
            triangle = {p1, p4, p3};
            triangle_normals = getTriangleNormals(triangle);
            vertices.insert(vertices.end(), triangle.begin(), triangle.end());
            normals.insert(normals.end(), triangle_normals.begin(), triangle_normals.end());
            
            texcoords.push_back(glm::vec2(0.0, 1.0));
            texcoords.push_back(glm::vec2(0.0, 0.0));
            texcoords.push_back(glm::vec2(1.0, 0.0));
        }
    }
        
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
    
    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
    
    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());
    
    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    
    std::cout << "Terrain is created with " << vertices.size() << " vertices\n";
    return mesh;
}

vector<vector< float > > generateHeightsWithPerlin(int x_size, int y_size, float scale) {
    vector<vector<float> > heights = vector<vector<float> >(x_size, vector<float>(y_size, 0.0));
    PerlinNoise pn;
    float max_val = -1e9, min_val = 1e9;
    for(int ix = 0; ix < heights.size(); ++ix) {
        for(int iy = 0; iy < heights[0].size(); ++iy) {
            heights[ix][iy] = pn.noise(scale * float(ix) / x_size, scale * float(iy) / y_size, 0.5);
            max_val = std::max(heights[ix][iy], max_val);
            min_val = std::min(heights[ix][iy], min_val);
        }
    }
    return heights;
}

vector<vector< float > > generateHeights(int x_size, int y_size, const vector<std::pair<int, int> > & top_points) {
    
    const double mean = 0.0;
    const double std = 0.04;
    
    std::default_random_engine generator;
    std::normal_distribution<double> dist(mean, std);
    vector<vector<float> > heights = vector<vector<float> >(x_size, vector<float>(y_size, 0.0));
    float min_val = 0.0;
    
    for(int ix = 1; ix < heights.size(); ++ix) {
        for(int iy = 1; iy < heights[0].size(); ++iy) {
            heights[ix][iy] = 0.3333 * (heights[ix-1][iy] + heights[ix][iy - 1] + heights[ix - 1][iy - 1])
            + dist(generator);
            
            for(int j = 0; j < top_points.size(); ++j) {
                int px = top_points[j].first;
                int py = top_points[j].second;
                
                heights[ix][iy] += 0.05 * (float)(px - ix) / (float)heights.size() / (float)top_points.size()
                                 + 0.05 * (float)(py - iy) / (float)heights[0].size() / (float)top_points.size();
            }
            
            min_val = std::min(heights[ix][iy], min_val);
        }
    }
    
    for(int ix = 0; ix < heights.size(); ++ix) {
        for(int iy = 0; iy < heights[0].size(); ++iy) {
            heights[ix][iy] -= min_val;
        }
    }
    return heights;
}
    
class TerrainApplication : public Application
{
public:
    MeshPtr _perlin_terrain;
    MeshPtr _terrain;
    MeshPtr _bunny;
    
    ShaderProgramPtr _shader_height;
    ShaderProgramPtr _shader_normal;
    ShaderProgramPtr _shader_white;
    
    void makeScene() override
    {
        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();
        
        // Perlin Terrain
        vector<vector< float > > heights = generateHeightsWithPerlin(1000, 1000, 20.0);
        _perlin_terrain = buildTerrainWithHeights(heights, std::make_pair(20.0 , 20.0));
        _perlin_terrain->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-10.0f, -10.0f, -0.3f)));
        
        // My Terrain
        vector<std::pair<int, int> > top_points = {
            std::make_pair(50,40),
        };
        heights = generateHeights(100, 100, top_points);
        _terrain = buildTerrainWithHeights(heights, std::make_pair(5.0 , 5.0));
        _terrain->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f , -1.0f)));
        
        //Создаем меш из файла
        _bunny = loadFromFile("497ZotovData/models/bunny.obj");
        _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 3.0f, 0.0f)));
        
        //Создаем шейдерную программу
        _shader_normal = std::make_shared<ShaderProgram>("497ZotovData/shaders/shaderNormal.vert",
                                                         "497ZotovData/shaders/shader.frag");
        _shader_height = std::make_shared<ShaderProgram>("497ZotovData/shaders/shaderHeight.vert",
                                                         "497ZotovData/shaders/shader.frag");
        _shader_white = std::make_shared<ShaderProgram>("497ZotovData/shaders/shaderWhite.vert",
                                                         "497ZotovData/shaders/shader.frag");
    }
    
    void update() override
    {
        Application::update();
    }
    
    void draw_with_shader(const MeshPtr & mesh, const ShaderProgramPtr & shader) const {
        //Устанавливаем шейдер.
        shader->use();
        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
        
        mesh->draw();
    }
    
    void draw() override
    {
        Application::draw();
        
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        
        glViewport(0, 0, width, height);
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        // Perlin
        draw_with_shader(_perlin_terrain, _shader_height);
        
        // Random Walk
        draw_with_shader(_terrain, _shader_height);
        
        //Bunny for fun
        draw_with_shader(_bunny, _shader_normal);
        
        
        glEnable(GL_POLYGON_OFFSET_LINE);
        glPolygonOffset(0.0, 1.0);
        
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        draw_with_shader(_terrain, _shader_white);
        
        glDisable(GL_POLYGON_OFFSET_LINE);
        
        
    }
};

int main()
{
    TerrainApplication app;
    app.start();
    
    return 0;
}
