#include "Parallelepiped.h"

#include <memory>
#include <vector>
#include <iostream>

glm::vec3 swapAxis_(glm::vec3 point) {
    return glm::vec3(point.z, point.x, point.y);
}

void addRect(
    std::vector<glm::vec3>& vertices,
    std::vector<glm::vec3>& normals,
    std::vector<glm::vec2>& texcoords,
    glm::vec3 A,
    glm::vec3 B,
    glm::vec3 C,
    glm::vec3 normal
) {
    // std::cout << A.x << " " << A.y << " " << A.z << std::endl;
    A = swapAxis_(A);
    // std::cout << A.x << " " << A.y << " " << A.z << std::endl << std::endl;
    B = swapAxis_(B);
    C = swapAxis_(C);
    normal = swapAxis_(normal);

    glm::vec3 D = A + C - B;
    vertices.push_back(A);
    vertices.push_back(B);
    vertices.push_back(C);
    vertices.push_back(A);
    vertices.push_back(C);
    vertices.push_back(D);

    for (size_t i = 0; i < 6; ++i)
        normals.push_back(normal);

    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
}


MeshPtr getDefaultParallelepiped(glm::vec3 diagonal) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    // add parallel planes through loop
    for (size_t i = 0; i < 2; ++i) {
        addRect(
            vertices,
            normals,
            texcoords,
            glm::vec3(-diagonal.x / 2, -diagonal.y / 2, i * diagonal.z),
            glm::vec3(diagonal.x / 2, -diagonal.y / 2, i * diagonal.z),
            glm::vec3(diagonal.x / 2, diagonal.y / 2, i * diagonal.z),
            glm::vec3(0, 0, 2 * i - 1)
        );
        addRect(
            vertices,
            normals,
            texcoords,
            glm::vec3(-diagonal.x / 2, (i - 0.5) * diagonal.y, 0),
            glm::vec3(diagonal.x / 2, (i - 0.5) * diagonal.y, 0),
            glm::vec3(diagonal.x / 2, (i - 0.5) * diagonal.y, diagonal.z),
            glm::vec3(0, 2 * i - 1, 0)
        );
        addRect(
            vertices,
            normals,
            texcoords,
            glm::vec3((i - 0.5)  * diagonal.x, -diagonal.y / 2, 0),
            glm::vec3((i - 0.5) * diagonal.x, diagonal.y / 2, 0),
            glm::vec3((i - 0.5) * diagonal.x, diagonal.y / 2, diagonal.z),
            glm::vec3(2 * i - 1, 0, 0)
        );
    }

    DataBufferPtr verticesBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    verticesBuffer->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr normalsBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    normalsBuffer->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr textureBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    textureBuffer->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, verticesBuffer);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, normalsBuffer);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, textureBuffer);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


MeshPtr getParallelepiped(const ParallelepipedParams& params) {
    float length = glm::distance(params.firstPoint, params.secondPoint);
    glm::vec3 firstPoint = swapAxis_(params.firstPoint);
    glm::vec3 secondPoint = swapAxis_(params.secondPoint);
    glm::vec3 center = swapAxis_(params.center);
    glm::vec3 tmpAxis = swapAxis_(params.axis);

    MeshPtr result = getDefaultParallelepiped(glm::vec3(params.width, params.thickness, length));

    glm::mat4 modelMatrix;
    modelMatrix = glm::translate(glm::mat4(1.0f), center);

    float distance = glm::distance(secondPoint, firstPoint);
    float cos = (secondPoint.x - firstPoint.x) / distance;
    float angle = std::acos(cos);
    glm::vec3 axis = glm::cross(glm::vec3(1, 0, 0), secondPoint - firstPoint);
    modelMatrix = glm::rotate(modelMatrix, params.angle, tmpAxis);
    modelMatrix = glm::translate(modelMatrix, firstPoint - center);
    modelMatrix = glm::rotate(modelMatrix, angle, axis);

    result->setModelMatrix(modelMatrix);
    return result;
}
