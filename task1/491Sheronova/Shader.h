#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>

class Shader
{
public:
	GLuint Program;
    	Shader(const GLchar* vertexPath, const GLchar* fragmentPath)
    	{
        	//  Retrieve the vertex/fragment source code from filePath
        	std::string vertexCode;
        	std::string fragmentCode;
        	std::ifstream vShaderFile;
        	std::ifstream fShaderFile;
        	vShaderFile.open(vertexPath);
            	fShaderFile.open(fragmentPath);
            	std::stringstream vShaderStream, fShaderStream;
            	vShaderStream << vShaderFile.rdbuf();
            	fShaderStream << fShaderFile.rdbuf();
        	vShaderFile.close();
            	fShaderFile.close();    
		vertexCode = vShaderStream.str();
            	fragmentCode = fShaderStream.str();
        	const GLchar* vShaderCode = vertexCode.c_str();
        	const GLchar * fShaderCode = fragmentCode.c_str();
        	//  Compile shaders
        	GLuint vertex, fragment;
        	vertex = glCreateShader(GL_VERTEX_SHADER);
        	glShaderSource(vertex, 1, &vShaderCode, NULL);
        	glCompileShader(vertex);
        	fragment = glCreateShader(GL_FRAGMENT_SHADER);
        	glShaderSource(fragment, 1, &fShaderCode, NULL);
        	glCompileShader(fragment);
        	this->Program = glCreateProgram();
        	glAttachShader(this->Program, vertex);
        	glAttachShader(this->Program, fragment);
        	glLinkProgram(this->Program);
        	glDeleteShader(vertex);
        	glDeleteShader(fragment);
    	}
    	void Use() 
    	{ 
        	glUseProgram(this->Program); 
    	}
};

#endif
