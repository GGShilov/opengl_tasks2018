set(SRC_FILES
    LSystem.cpp
    Main.cpp
    cylinder.cpp
		common/DebugOutput.cpp
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
)

include_directories(common)
MAKE_TASK(491pilyugin 1 "${SRC_FILES}")
